#!/bin/bash -e

# bunnysign is a dumb take on cowsay, but without a cow... and without saying.

COLOR_RED='\033[0;31m'
COLOR_RED_BRIGHT='\033[1;31m'
COLOR_GREEN='\033[0;32m'
COLOR_GREEN_BRIGHT='\033[1;32m'
COLOR_BLUE='\033[0;34m'
COLOR_BLUE_BRIGHT='\033[1;34m'
COLOR_YELLOW='\033[0;33m'
COLOR_YELLOW_BRIGHT='\033[1;33m'
COLOR_BLACK='\033[0;30m'
COLOR_WHITE='\033[1;37m'
COLOR_NONE='\033[0m'
COLOR_CYAN='\033[0;36m'
FONT_BOLD='\033[1m'

FONT_ITALIC='\033[3m'

FONT_UNDERLINE='\e[4m'

FONT_NORMAL='\e[21m\e[24m'
FONT_RESET="${FONT_NORMAL}${COLOR_NONE}"

# Remove colors you don't like here 🤷
RAINBOW_COLORS=(
    ${COLOR_RED}
    ${COLOR_RED_BRIGHT}
    ${COLOR_GREEN}
    ${COLOR_GREEN_BRIGHT}
    ${COLOR_BLUE}
    ${COLOR_BLUE_BRIGHT}
    ${COLOR_YELLOW}
    ${COLOR_YELLOW_BRIGHT}
    ${COLOR_BLACK}
    ${COLOR_WHITE}
    ${COLOR_NONE}
    ${COLOR_CYAN}
)

RAINBOW_IDX=0

updateLineColor() {
    FONT_COLOR=${RAINBOW_COLORS[RAINBOW_IDX]}
    RAINBOW_IDX=$(((${RAINBOW_IDX} + 1) % ${#RAINBOW_COLORS[@]}))
    if [ ${RAINBOW_IDX} == 0 ]; then
        RAINBOW_COLORS=( $(shuf -e "${RAINBOW_COLORS[@]}") )
    fi
}

setDefaultParameters() {
    # Default Parameters
    CONTENT_WIDTH=40
    MINIMUM_WIDTH=8
    SIGN_PREFIX="| "
    SIGN_SUFFIX=" |"
    SIGN_CORNER="|"
    SIGN_TOP="-"
    SIGN_BOT="-"
    HUNTING_SEASON="DUCK"
    SPLIT_BY="SMART"
    FONT_COLOR=""
    FONT_COLOR_END=""
    LOLCAT=false
    ENCRYPTION=()
    INTERACTIVE=false
    TRANSPOSE=false
    # Adjust the sign position
    # if the message is too small
    SIGN_LEFT_EDGE_PADDING=""
    WORKERS=1

    # Bunny Lego®
    LEFT_EYE='•'
    RIGHT_EYE='•'
    MOUTH_NOSE_THING='ㅅ'
}

# Prints the top/botton part of the sign
printBotTopSign() {
    local CHAR="$1"
    local COUNT="$2"

    # Some magical shit
    echo "${SIGN_LEFT_EDGE_PADDING}${SIGN_CORNER}$(printf %$((COUNT+2))s |tr " " "${CHAR}")${SIGN_CORNER}"
}

mountHelpMessage() {
    local IFS=$'\n'

    HELP_MESSAGE_LINES=(" Usag(i)e:")
    HELP_MESSAGE_LINES+=("         $0 [A BUNCH OF OPTIONS]... [YOUR MESSAGE HERE]")

    # This is just to avoid awking itself
    HELP_BUNNY_START_TAG="BUNNY""_HELP"
    HELP_BUNNY_END_TAG="PLEH""_YNNUB"
    HELP_MESSAGE_LINES+=( $(awk "/${HELP_BUNNY_START_TAG}/{f=1;next} /${HELP_BUNNY_END_TAG}/{f=0} f" $0 | sed 's/#//') )

    MAX_HELP_LINE_SIZE=0
    for HELP_LINE in ${HELP_MESSAGE_LINES[@]}; do
        if [ ${#HELP_LINE} -gt ${MAX_HELP_LINE_SIZE} ]; then
            MAX_HELP_LINE_SIZE=${#HELP_LINE}
        fi
    done
}

printMessage() {
    # MESSAGE
    for LINE in "$@"; do
        LINE_LEN=${#LINE}
        PADDING_LEN=$((CONTENT_WIDTH-LINE_LEN))
        if [ -n "${CENTER}" ]; then
            PADDING_L_LEN=$((PADDING_LEN/2))
            PADDING_R_LEN=$((PADDING_LEN/2+PADDING_LEN%2))
            PADDING_L="$(printf %${PADDING_L_LEN}s)"
            PADDING_R="$(printf %${PADDING_R_LEN}s)"
        else
            PADDING_R="$(printf %${PADDING_LEN}s)"
        fi

        if [[ ${LOLCAT} == true ]]; then
            updateLineColor
        fi

        for ENC in "${ENCRYPTION[@]}"; do
            case "${ENC}" in
                "ROT13")
                    LINE=$( echo "${LINE}" | tr '[A-Za-z]' '[N-ZA-Mn-za-m]' )
                    ;;
                "REVERSE")
                    LINE=$( echo "${LINE}" | rev )
                    ;;
                *)
                    LINE=$(tr -dc A-Za-z0-9 </dev/urandom | head -c ${#LINE}; echo '')
                    ;;
            esac
        done

        echo -e "${SIGN_LEFT_EDGE_PADDING}${SIGN_PREFIX}${PADDING_L}${FONT_COLOR}${LINE}${FONT_COLOR_END}${PADDING_R}${SIGN_SUFFIX}"
    done
}

bunnysign() {
    # Get piped data
    if [[ ! -t 0 ]]; then
        while read -r PIPED; do
            MESSAGE+="$PIPED"
        done;
    fi

    # Adapt Sign size to content if message is small enough
    [ -z "${FIXED_WIDTH}" ] && [ ${#MESSAGE} -lt ${CONTENT_WIDTH} ] &&
        [ ${#MESSAGE} -gt ${MINIMUM_WIDTH} ] && CONTENT_WIDTH=${#MESSAGE}

    case "${SPLIT_BY}" in
        WORD)
            MESSAGE_LINES=(${MESSAGE})
            ;;
        LENGTH)
            MESSAGE_LINES=()
            POSITION=0
            while [ "${POSITION}" -lt ${#MESSAGE} ]; do
                MESSAGE_LINES+=("${MESSAGE:POSITION:CONTENT_WIDTH}")
                POSITION=$((POSITION+CONTENT_WIDTH))
            done
            ;;
        SMART)
            WORDS=(${MESSAGE})
            MESSAGE_LINES=()
            while [ "${#WORDS[@]}" -gt 0 ]; do
                CURRENT_LINE=""
                while [ true ]; do
                    # Update lengths for this iteration
                    CURRENT_LINE_LEN=${#CURRENT_LINE}
                    CURRENT_WORD_LEN=${#WORDS[0]}

                    # Consider the separator in the calculation
                    [ -n "${CURRENT_LINE}" ] && CURRENT_LINE_LEN="$((CURRENT_LINE_LEN+1))"

                    # Stop if CURRENT_LINE would exceed the limit or if there are no more words
                    if [ "$((CURRENT_LINE_LEN+CURRENT_WORD_LEN))" -gt "$CONTENT_WIDTH" ] ||
                           [ "${#WORDS[@]}" -le 0 ]; then

                        # Check if current line isn't empty
                        if [ -z "${CURRENT_LINE}" ]; then
                            # Split the word
                            CURRENT_LINE="${WORDS[0]:0:CONTENT_WIDTH}"
                            WORDS[0]="${WORDS[0]:CONTENT_WIDTH}"
                        fi
                        break
                    fi

                    # Conditionally add a word separator
                    [ -n "${CURRENT_LINE}" ] && CURRENT_LINE+=" "

                    # Concatenate the current word and remove it from the list
                    CURRENT_LINE+="${WORDS[0]}"
                    WORDS=("${WORDS[@]:1}")
                done
                MESSAGE_LINES+=("${CURRENT_LINE}")
            done
            ;;
        HELP_MESSAGE)
            MESSAGE_LINES=("${HELP_MESSAGE_LINES[@]}")
            CONTENT_WIDTH=${MAX_HELP_LINE_SIZE}
            FIXED_WIDTH="TRUE"
            ;;
    esac

    if [ -z "${FIXED_WIDTH}" ]; then
        CONTENT_WIDTH=${MINIMUM_WIDTH}
        for LINE in "${MESSAGE_LINES[@]}"; do
            [ ${#LINE} -gt ${CONTENT_WIDTH} ] && CONTENT_WIDTH=${#LINE}
        done
    fi

    local CONTENT_WIDTH="${CONTENT_WIDTH}"
    local MESSAGE_LINES=( "${MESSAGE_LINES[@]}" )

    # What was this becomes that, and what was that becomes this
    # No need for more explanations than that (or this ? 😵)
    if [[ ${TRANSPOSE} == true ]]; then
        CONTENT_HEIGHT="${CONTENT_WIDTH}"
        CONTENT_WIDTH="${#MESSAGE_LINES[@]}"

        NEW_MESSAGE_LINES=()

        for i in $(seq 0 $(( ${CONTENT_HEIGHT} - 1 ))); do
            NEW_LINE=""
            for j in $(seq 0 $(( ${CONTENT_WIDTH} - 1 ))); do
                NEW_CHAR="${MESSAGE_LINES[j]:i:1}"
                if [ "${NEW_CHAR}" == "" ]; then
                    NEW_CHAR=" "
                fi

                NEW_LINE+="${NEW_CHAR}"

                # Add column spaces separator if this isn't the last
                if [ "$j" -lt "$(( ${CONTENT_WIDTH} - 1 ))" ]; then
                    NEW_LINE+=" "
                fi

            done
            NEW_MESSAGE_LINES+=( "${NEW_LINE}" )
        done

        MESSAGE_LINES=( "${NEW_MESSAGE_LINES[@]}" )

        # Recalculate the CONTENT_WIDTH cause we added a lot of spaces
        CONTENT_WIDTH=0
        for LINE in "${MESSAGE_LINES[@]}"; do
            if [ ${#LINE} -gt ${CONTENT_WIDTH} ]; then
                CONTENT_WIDTH=${#LINE}
            fi
        done

        # If the sign width is too little, we need to move it a little
        if [ "${CONTENT_WIDTH}" -le 9 ]; then
            # From trial and error, a good value is follows the
            # following line equation:
            #
            # padding = -CONTENT_WIDTH/2 + 11/2
            local PADDING_LEN=$(( (11 - "${CONTENT_WIDTH}") / 2 ))
            SIGN_LEFT_EDGE_PADDING="$(printf %${PADDING_LEN}s)"
        fi
    fi

    # TOP SIGN 🔝👌
    printBotTopSign ${SIGN_TOP} ${CONTENT_WIDTH}

    MESSAGE_CHUNK_SIZE=$(( ${#MESSAGE_LINES[@]} / WORKERS ))

    for WORKER in $(seq 0 $(( ${WORKERS} - 1 ))); do
        local IDX=$(( ${WORKER} * ${MESSAGE_CHUNK_SIZE} ))
        if [ "${WORKER}" -lt "$(( ${WORKERS} - 1))" ]; then
            CHUNK=( "${MESSAGE_LINES[@]:${IDX}:${MESSAGE_CHUNK_SIZE}}" )
        else
            CHUNK=( "${MESSAGE_LINES[@]:${IDX}}" )
        fi
        printMessage "${CHUNK[@]}" &
    done

    wait

    # BOTTON SIGN
    printBotTopSign ${SIGN_BOT} ${CONTENT_WIDTH}

    # The Bunny, finally
    if [ "${HUNTING_SEASON}" != "BUNNY" ]; then
        BUNNY="(\\__/) ||\n(${LEFT_EYE}${MOUTH_NOSE_THING}${RIGHT_EYE}) ||\n/ 　 づ\n"
    fi

    echo -e -n ${BUNNY}

}

# =============================================================================
# Main™
# =============================================================================

setDefaultParameters

# Argument parsing
# BUNNY_HELP
#
# Flags:
# PLEH_YNNUB
while [[ ${#} -gt 0 ]]; do
    case "${1}" in
        # BUNNY_HELP
        # -c, --center, --center-text
        #     Centers the... text
        #
        # PLEH_YNNUB
        -c|--center|--center-text)
            CENTER="TRUE"
            shift # past argument
            ;;
        # BUNNY_HELP
        # -d, --dead
        #     Poor bunny, it had so much to live
        #
        # PLEH_YNNUB
        -d|--dead)
            LEFT_EYE="ˣ"
            RIGHT_EYE="ˣ"
            MOUTH_NOSE_THING="__"
            shift # past argument
            ;;
        # BUNNY_HELP
        # -s, --surprised
        #     Hey look, it's Xmas!
        #
        # PLEH_YNNUB
        -s|--surprised)
            LEFT_EYE=" °"
            RIGHT_EYE="°"
            MOUTH_NOSE_THING="o"
            shift # past argument
            ;;
        # BUNNY_HELP
        # -w, --width <width>
        #     On a tidy budget? Fix the sign
        #     width, that should resolve it
        #
        # PLEH_YNNUB
        -w|--width)
            CONTENT_WIDTH="$2"
            FIXED_WIDTH="TRUE"
            shift # past argument
            shift # past value
            ;;
        # BUNNY_HELP
        # -n, --no-bunny, --bunnyless
        #     DUCK SEASON? NO, IT IS RABBIT SEASON!
        #
        # PLEH_YNNUB
        -n|--no-bunny|--bunnyless)
            HUNTING_SEASON="BUNNY"
            shift # past argument
            ;;
        # BUNNY_HELP
        # --smart-split
        #     Split the message smartly, I guess
        #
        # PLEH_YNNUB
        --smart-split)
            SPLIT_BY="SMART"
            shift # past argument
            ;;
        # BUNNY_HELP
        # --split-by-word
        #     Wordly split the message
        #
        # PLEH_YNNUB
        --split-by-word)
            SPLIT_BY="WORD"
            shift # past argument
            ;;
        # BUNNY_HELP
        # --split-by-length
        #     Split the message by L E N G T H
        #
        # PLEH_YNNUB
        --split-by-length)
            SPLIT_BY="LENGTH"
            shift # past argument
            ;;
        # BUNNY_HELP
        # --color <color>
        #     Orange is the new black, isn't it?
        #
        # PLEH_YNNUB
        --color)
            FOUND_COLOR=true
            # BUNNY_HELP
            # Supported colors:
            # PLEH_YNNUB
            case "${2}" in
                # BUNNY_HELP
                # - red
                # PLEH_YNNUB
                "red")
                    FONT_COLOR=${COLOR_RED}
                    ;;
                # BUNNY_HELP
                # - red-bold
                # PLEH_YNNUB
                "red-bold")
                    FONT_COLOR=${COLOR_RED_BRIGHT}
                    ;;
                # BUNNY_HELP
                # - green
                # PLEH_YNNUB
                "green")
                    FONT_COLOR=${COLOR_GREEN}
                    ;;
                # BUNNY_HELP
                # - green-bold
                # PLEH_YNNUB
                "green-bold")
                    FONT_COLOR=${COLOR_GREEN_BRIGHT}
                    ;;
                # BUNNY_HELP
                # - blue
                # PLEH_YNNUB
                "blue")
                    FONT_COLOR=${COLOR_BLUE}
                    ;;
                # BUNNY_HELP
                # - blue-bold
                # PLEH_YNNUB
                "blue-bold")
                    FONT_COLOR=${COLOR_BLUE_BRIGHT}
                    ;;
                # BUNNY_HELP
                # - yellow
                # PLEH_YNNUB
                "yellow")
                    FONT_COLOR=${COLOR_YELLOW}
                    ;;
                # BUNNY_HELP
                # - yellow-bold
                # PLEH_YNNUB
                "yellow-bold")
                    FONT_COLOR=${COLOR_YELLOW_BRIGHT}
                    ;;
                # BUNNY_HELP
                # - black
                # PLEH_YNNUB
                "black")
                    FONT_COLOR=${COLOR_BLACK}
                    ;;
                # BUNNY_HELP
                # - white
                # PLEH_YNNUB
                "white")
                    FONT_COLOR=${COLOR_WHITE_BRIGHT}
                    ;;
                # BUNNY_HELP
                # - rainbow
                # PLEH_YNNUB
                "rainbow")
                    RAINBOW_COLORS=( $(shuf -e "${RAINBOW_COLORS[@]}") )
                    LOLCAT=true
                    ;;
                *)
                    FOUND_COLOR=false
                    ;;
            esac

            if [[ ${FOUND_COLOR} == false ]]; then
                MESSAGE="I don't know this color: \"$2\""
                break # stop parsing
            else
                FONT_COLOR_END=${FONT_RESET}
                shift # past argument
            fi

            shift # past value
            ;;
        # BUNNY_HELP
        #
        # --rot13
        #     The best encryption algorithm
        #     known to mankind
        #
        # PLEH_YNNUB
        --rot13)
            ENCRYPTION+=("ROT13")
            shift # past argument
            ;;
        # BUNNY_HELP
        # -r, --rev, --reverse
        #     Do you need a mirror?
        #
        # PLEH_YNNUB
        -r|--rev|--reverse)
            ENCRYPTION+=("REVERSE")
            shift # past argument
            ;;
        # BUNNY_HELP
        # --interactive
        #    Static signs are so... static
        #    (Press <esc> to quit)
        #
        # PLEH_YNNUB
        --interactive)
            INTERACTIVE=true
            shift # past argument
            ;;
        # BUNNY_HELP
        # -t, --transpose
        #    A^t
        #
        # PLEH_YNNUB
        -t|--transpose)
            TRANSPOSE=true
            shift # past argument
            ;;
        # BUNNY_HELP
        # --threads <n>
        #    I am speed
        #
        # PLEH_YNNUB
        --threads)
            WORKERS="$2"
            shift # past argument
            shift # past value
            ;;
        # BUNNY_HELP
        # -h, --help
        #     This long long message you are
        #     seeing right now
        #
        # PLEH_YNNUB
        -h|--help)
            mountHelpMessage
            # Let's avoid problems
            INTERACTIVE=false
            SPLIT_BY="HELP_MESSAGE"
            break
            ;;
        -?|--*)
            setDefaultParameters
            MESSAGE="I don't know any \"$1\""
            break # stop parsing
            ;;
        -*)
            setDefaultParameters
            MESSAGE="Did you mean \"-$1\"?"
            break # stop parsing
            ;;
        *) # message part
            if [ -z "${MESSAGE}" ]; then
                # If the message isn't defined
                # start a new message
                MESSAGE="${1}"
            else
                # If the message is defined
                # concatenate the new content
                MESSAGE+=" ${1}"
            fi
            shift # past argument
            ;;
    esac
done

if [[ ${INTERACTIVE} == false ]]; then
    bunnysign
else
    FINISHED=false
    while [[ ${FINISHED} == false ]]; do
        read -rN1 CHAR
        CHAR_ASCII=$(echo -n -e $CHAR | od -An -t dC)

        # Esc
        if [[ ${CHAR_ASCII} -eq 27 ]]; then
            # Instead of breaking it here, we need to reprint the sign
            # one more time to clean up the ^[ input. That's why I'm
            # using a flag here
            FINISHED=true

        # Backspace
        elif [[ ${CHAR_ASCII} -eq 127 ]]; then
            if [ ${#MESSAGE} -gt 0 ]; then
                MESSAGE="${MESSAGE::-1}"
            fi

        # Anything else
        else
            MESSAGE+="${CHAR}"

        fi

        # Clear the screen
        printf "\033c";

        # Reprint the bunny
        bunnysign
    done
fi
